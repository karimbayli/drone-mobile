import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Tab2Service {

  constructor(private http: HttpClient) { }

  getDronesByRadius(coords, radius) {
    return this.http.get(`${environment.host}/v1/data-live/firelog?lat=${coords.lat}&lon=${coords.lng}&r=${radius}`)
  }

  reportDrone(data) {
    return this.http.post(`${environment.host}/v1/drone/report`, {
      drone_id: data.drone_id,
      latitude: data.lat,
      longitude: data.lng,
      message: data.message
    })
  }
}
