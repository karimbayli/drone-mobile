import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Tab2Service } from '../tab2.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  public reportForm: FormGroup;
  @Input() droneId: number
  @Input() lat: number
  @Input() lng: number

  constructor(
    public modalController: ModalController,
    private formBuilder: FormBuilder,
    private tab2Service: Tab2Service
  ) {

  }

  ngOnInit() {
    console.log('ngOnInit', this.droneId)

    this.reportForm = this.formBuilder.group({
      drone_id: [this.droneId],
      lat: [this.lat],
      lng: [this.lng],
      message: ['', [Validators.required]],
    })
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }
  send() {
    console.log('sending', this.reportForm.value)
    this.tab2Service.reportDrone(this.reportForm.value).subscribe(response => {
      this.dismiss()
    })
  }

}
