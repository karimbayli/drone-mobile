import { AfterContentInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GeolocationPosition, Plugins } from '@capacitor/core';
import { environment } from 'src/environments/environment';
import { Tab2Service } from './tab2.service';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from './modal/modal.component';

const { Geolocation } = Plugins;

declare var google;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements AfterContentInit, OnInit {
  secure: boolean = true
  public map;
  public circle;
  markers = []
  radius: number
  mycoordinates = {}
  ws: WebSocket = new WebSocket(environment.ws);

  @ViewChild('mapElement', { static: true }) mapElement: ElementRef;
  constructor(
    private tab2Service: Tab2Service,
    public modalController: ModalController
  ) {
    this.ws.onopen = function () {
      console.log('conntected')
    };
    this.ws.onmessage = (event) => {
      console.log(JSON.stringify(event.data))
      let data = JSON.parse(event.data)
      let distance = this.getDistance({ lat: data.latitude, lng: data.longitude }, this.mycoordinates)
      if (distance > this.radius) {
        this.removeMarker(data.drone_id)
      } else {
        this.setMarker(data.drone_id,
          { lat: parseFloat(data.latitude), lng: parseFloat(data.longitude) },
          data.angle
        )
      }

      console.log(this.markers.length, this.secure)
      if (this.markers.length && this.secure) {
        this.secure = false
        this.circle.setOptions({
          strokeColor: "#FF0000",
          fillColor: "#FF0000"
        });
      } else if (!this.markers.length && !this.secure) {
        this.secure = true
        this.circle.setOptions({
          fillColor: '#3a7124c7',
          strokeColor: '#3a7124c7'
        });
      }

    };

    this.ws.onclose = function () {
      console.log('closed')
    };
  }

  rad(x) {
    return x * Math.PI / 180;
  };

  getDistance(p1, p2) {
    const R = 6378137; // Earth’s mean radius in meter
    let dLat = this.rad(p2.lat - p1.lat);
    let dLong = this.rad(p2.lng - p1.lng);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.rad(p1.lat)) * Math.cos(this.rad(p2.lat)) *
      Math.sin(dLong / 2) * Math.sin(dLong / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    return d; // returns the distance in meter
  };

  async presentModal(props) {
    console.log('presentModal', props)
    const modal = await this.modalController.create({
      component: ModalComponent,
      cssClass: 'my-custom-class',
      componentProps: props
    });
    return await modal.present();
  }

  getDroneIcon(color, degree = 45) {
    let svg = `<?xml version="1.0" encoding="iso-8859-1"?><!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"	y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">	<g>		<g>			<polygon transform="rotate(${degree},250,250)" fill="${color}" points="512,255.709 512,176.558 308.253,176.558 308.253,47.192 256,1.172 203.747,47.193 203.747,176.559 0,176.559 			0,255.711 203.747,279.93 203.747,349.021 163.878,349.021 163.878,382.413 208.79,382.413 232.074,445.747 194.134,445.747 			194.134,479.138 244.35,479.138 256,510.828 267.65,479.137 317.866,479.137 317.866,445.746 279.926,445.746 303.21,382.412 			348.122,382.412 348.122,349.02 308.253,349.02 308.253,279.93 		" />		</g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g></svg>`
    return {
      url: 'data:image/svg+xml;base64,' + btoa(svg),
      scaledSize: new google.maps.Size(40, 40)
    }
  }

  ngOnInit(): void {
  }

  changeRadius(event) {
    this.radius = event.target.value
    this.circle.setRadius(event.target.value)
    this.map.fitBounds(this.circle.getBounds(), 0);
  }
  getMarker(id) {
    for (let marker of this.markers) {
      if (marker.get('id') == id) {
        return marker
      }
    }
  }

  removeMarker(id) {
    let found
    for (let marker of this.markers) {
      if (marker.get('id') == id) {
        found = marker
        break
      }
    }
    if (found) {
      found.setMap(null)
      this.markers.splice(this.markers.indexOf(found), 1)
    }
  }
  setMarker(id, position, angle) {
    let found = false
    for (let marker of this.markers) {
      if (marker.get('id') == id) {
        found = true
        marker.setPosition(position)
        marker.setIcon(this.getDroneIcon('blue', angle),)
      }
    }
    if (!found) {
      let marker = new google.maps.Marker({
        position: position,
        map: this.map,
        icon: this.getDroneIcon('blue', angle),
        title: 'Drone'
      });

      marker.addListener("click", () => {
        this.presentModal({
          droneId: id,
          lat: position.lat,
          lng: position.lng
        })
      });
      marker.set('id', id)
      this.markers.push(marker)
    }
  }

  ngAfterContentInit(): void {
    Geolocation.getCurrentPosition().then((position: GeolocationPosition) => {
      this.mycoordinates = { lat: position.coords.latitude, lng: position.coords.longitude }
      this.map = new google.maps.Map(
        this.mapElement.nativeElement, {
        mapId: 'b4c5a24a02d7a02f',
        center: this.mycoordinates,
        useStaticMap: false,
        fullscreenControl: false,
        zoomControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        disableDefaultUI: false,
      });
      this.circle = new google.maps.Circle({
        strokeColor: "#3a7124c7",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#3a7124c7",
        fillOpacity: 0.35,
        map: this.map,
        center: this.mycoordinates,
        radius: 2000,
      });
      this.map.fitBounds(this.circle.getBounds(), 0);
      new google.maps.Marker({
        position: this.mycoordinates,
        map: this.map,
        title: "Hello World!",
      });

      this.tab2Service.getDronesByRadius(this.mycoordinates, 2000)
        .subscribe(data => { console.log(data) })
    })

  }
}
