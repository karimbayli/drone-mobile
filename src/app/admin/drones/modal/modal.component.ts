import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  form: FormGroup
  constructor(
    public modalController: ModalController,
    private formBuilder: FormBuilder,
  ) {

  }

  ngOnInit() {

    this.form = this.formBuilder.group({
      imei: [''],
      owner: [''],
    })
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }
  send() {
    this.dismiss()
  }

}
