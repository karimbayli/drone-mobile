import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DronesPage } from './drones.page';

describe('DronesPage', () => {
  let component: DronesPage;
  let fixture: ComponentFixture<DronesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DronesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DronesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
