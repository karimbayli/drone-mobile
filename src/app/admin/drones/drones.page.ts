import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DroneService } from './drone.service';
import { ModalComponent } from './modal/modal.component';

@Component({
  selector: 'app-drones',
  templateUrl: './drones.page.html',
  styleUrls: ['./drones.page.scss'],
})
export class DronesPage implements OnInit {
  drones
  constructor(
    private droneService: DroneService,
    private modalController: ModalController
  ) {
    this.droneService.getDrones().subscribe((data: any) => {
      this.drones = data
    })
  }

  async register() {
    const modal = await this.modalController.create({
      component: ModalComponent
    })
    return await modal.present();
  }

  ngOnInit() {
  }

}
