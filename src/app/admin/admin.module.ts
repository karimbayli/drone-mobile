import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminPageRoutingModule } from './admin-routing.module';

import { AdminPage } from './admin.page';
import { DronesPage } from './drones/drones.page';
import { ReportsPage } from './reports/reports.page';
import { AccessesPage } from './accesses/accesses.page';
import { SimulationPage } from './simulation/simulation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminPageRoutingModule
  ],
  declarations: [AdminPage, DronesPage, ReportsPage, AccessesPage, SimulationPage],
})
export class AdminPageModule { }
