import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SimulationService {
  ws: WebSocket
  private deg2rad(degrees) {
    var pi = Math.PI;
    return degrees * (pi / 180);
  }

  private rad2deg(radians) {
    var pi = Math.PI;
    return radians * (180 / pi);
  }

  public getNewCordinates(latitude, longtitude, speed, agle, intervalTime) {
    agle = this.deg2rad(agle)
    const radiusEarthKilometres = 6371.01;

    let kmDistance = speed * (intervalTime / 1000.0) / 3600.0;

    var distRatio = kmDistance / radiusEarthKilometres;
    var distRatioSine = Math.sin(distRatio);
    var distRatioCosine = Math.cos(distRatio);

    var startLatRad = this.deg2rad(latitude);
    var startLonRad = this.deg2rad(longtitude);

    var startLatCos = Math.cos(startLatRad);
    var startLatSin = Math.sin(startLatRad);

    var endLatRads = Math.asin((startLatSin * distRatioCosine) + (startLatCos * distRatioSine * Math.cos(agle)));

    var endLonRads = startLonRad
      + Math.atan2(Math.sin(agle) * distRatioSine * startLatCos,
        distRatioCosine - startLatSin * Math.sin(endLatRads));

    let newLat = this.rad2deg(endLatRads);
    let newLong = this.rad2deg(endLonRads);
    return {
      lat: newLat,
      lng: newLong
    }
  }
  sendCoordinates(id, latitude, longitude, angle) {
    this.ws.send(JSON.stringify({
      angle,
      drone_id: id,
      latitude: latitude,
      longitude: longitude
    }))
  }

  constructor(private http: HttpClient) {
    this.ws = new WebSocket(environment.ws);
    this.ws.onopen = function () {
      console.log('simulator socket conntected')
    };
    this.ws.onclose = function () {
      console.log('simulator socket closed')
    };
  }

}
