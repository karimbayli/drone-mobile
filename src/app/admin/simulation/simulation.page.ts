import { AfterContentInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GeolocationPosition, Plugins } from '@capacitor/core';
import { SimulationService } from './simulation.service';
const { Geolocation } = Plugins;

declare var google;

@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.page.html',
  styleUrls: ['./simulation.page.scss'],
})
export class SimulationPage implements OnInit, AfterContentInit {
  public map;
  public circle;
  interval: any;
  running: boolean = false;
  droneMarker: any;
  private drone: any = {}
  angle: number = 45
  flightPath: any
  currentDrone = 1
  @ViewChild('simulationMap', { static: true }) simulationMap: ElementRef;

  constructor(private simulationService: SimulationService) { }
  droneChange(event) {
    this.currentDrone = event.target.value
  }
  getDroneIcon(color, degree = 45) {
    let svg = `<?xml version="1.0" encoding="iso-8859-1"?><!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"	y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">	<g>		<g>			<polygon transform="rotate(${degree},250,250)" fill="${color}" points="512,255.709 512,176.558 308.253,176.558 308.253,47.192 256,1.172 203.747,47.193 203.747,176.559 0,176.559 			0,255.711 203.747,279.93 203.747,349.021 163.878,349.021 163.878,382.413 208.79,382.413 232.074,445.747 194.134,445.747 			194.134,479.138 244.35,479.138 256,510.828 267.65,479.137 317.866,479.137 317.866,445.746 279.926,445.746 303.21,382.412 			348.122,382.412 348.122,349.02 308.253,349.02 308.253,279.93 		" />		</g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g>	<g>	</g></svg>`
    return {
      url: 'data:image/svg+xml;base64,' + btoa(svg),
      scaledSize: new google.maps.Size(40, 40)
    }
  }

  ngOnInit(): void {
  }


  setMarker(position) {
    let marker = new google.maps.Marker({
      position: position,
      map: this.map,
      icon: this.getDroneIcon('blue'),
      title: 'Drone'
    });
    return marker
  }

  ngAfterContentInit(): void {
    Geolocation.getCurrentPosition().then((position: GeolocationPosition) => {
      this.drone.coords = { lat: position.coords.latitude, lng: position.coords.longitude }
      this.map = new google.maps.Map(
        this.simulationMap.nativeElement, {
        mapId: 'b4c5a24a02d7a02f',
        center: this.drone.coords,
        useStaticMap: false,
        fullscreenControl: false,
        zoom: 15,
        zoomControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        disableDefaultUI: false,
      });

      this.droneMarker = this.setMarker(this.drone.coords)
      this.flightPath = new google.maps.Polyline({
        path: [this.drone.coords, this.drone.coords],
        geodesic: true,
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 2
      });
      this.flightPath.setMap(this.map)
    })
  }
  stop() {
    this.running = false
    clearInterval(this.interval)
  }
  start() {
    let intervalTime = 300
    this.running = true
    this.interval = setInterval(() => {
      this.drone.coords = this.simulationService
        .getNewCordinates(this.drone.coords.lat, this.drone.coords.lng, 50, this.angle, intervalTime)
      this.droneMarker.setPosition(this.drone.coords)
      this.map.setCenter(this.drone.coords)
      this.flightPath.getPath().push(new google.maps.LatLng(this.drone.coords.lat, this.drone.coords.lng))
      this.simulationService.sendCoordinates(this.currentDrone, this.drone.coords.lat, this.drone.coords.lng, this.angle)
    }, intervalTime)
  }
  increaseAngle() {
    this.angle += 10
    this.droneMarker.setIcon(this.getDroneIcon('blue', this.angle))
  }
  decreaseAngle() {
    this.angle -= 10
    this.droneMarker.setIcon(this.getDroneIcon('blue', this.angle))
  }
}
