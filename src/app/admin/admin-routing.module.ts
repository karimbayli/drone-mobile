import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AccessesPage } from './accesses/accesses.page';

import { AdminPage } from './admin.page';
import { DronesPage } from './drones/drones.page';
import { ReportsPage } from './reports/reports.page';
import { SimulationPage } from './simulation/simulation.page';


const routes: Routes = [
  {
    path: '',
    component: AdminPage,
    children: [
      {
        path: 'accesses',
        component: AccessesPage
      },
      {
        path: 'reports',
        component: ReportsPage
      },
      {
        path: 'drones',
        component: DronesPage
      },
      {
        path: 'simulation',
        component: SimulationPage
      },
      {
        path: '',
        redirectTo: 'reports',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPageRoutingModule { }
