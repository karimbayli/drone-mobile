import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-accesses',
  templateUrl: './accesses.page.html',
  styleUrls: ['./accesses.page.scss'],
})
export class AccessesPage implements OnInit {
  accesses = [{
    latitude: 40.3856709,
    longitude: 49.8856369,
    access: false
  }, {
    latitude: 40.3856709,
    longitude: 49.8856369,
    access: false
  }, {
    latitude: 40.3856709,
    longitude: 49.8856369,
    access: false
  }]
  constructor() { }

  setAccess(item, access) {
    item.access = access
  }

  ngOnInit() {
  }

}
