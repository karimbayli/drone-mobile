import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AccessesPage } from './accesses.page';

describe('AccessesPage', () => {
  let component: AccessesPage;
  let fixture: ComponentFixture<AccessesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AccessesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
