import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {
  public selected: string
  public pages = [
    {
      title: 'Reports',
      icon: 'flag',
      url: '/admin/reports'
    },
    {
      title: 'Drones',
      icon: 'airplane',
      url: '/admin/drones'
    },
    {
      title: 'Accesses',
      icon: 'accessibility',
      url: '/admin/accesses'
    },
    {
      title: 'Simulation',
      icon: 'map',
      url: '/admin/simulation'
    },
    {
      title: 'Exit',
      icon: 'exit',
      url: '/tabs/map'
    }
  ]

  change(title) {
    this.selected = title
    this.menuController.close()
  }
  constructor(private menuController: MenuController) {
  }


  ngOnInit() {

  }

}
