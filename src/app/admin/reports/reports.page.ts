import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { ImagemodalComponent } from './imagemodal/imagemodal.component';
import { ReportService } from './report.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.page.html',
  styleUrls: ['./reports.page.scss'],
})
export class ReportsPage implements OnInit {
  reports = []
  constructor(private reportService: ReportService,
    public modalController: ModalController
  ) {
    this.reportService.getList().subscribe((data: any) => {
      this.reports = data.reverse()
    })
  }
  async showImage(report) {
    const modal = await this.modalController.create({
      component: ImagemodalComponent,
      componentProps: {
        image: environment.host + '/v1/file/' + report.img_path
      }
    })
    return await modal.present();
  }
  ngOnInit() {
  }

}
