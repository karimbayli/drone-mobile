import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginError: boolean = false;
  public loginForm: FormGroup;

  constructor(private authService: AuthService,
    private router: Router,
    formBuilder: FormBuilder) {
    this.loginForm = formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }

  ngOnInit() { }

  handle(error) {
    if (error.status == 401) {
      console.log(this.loginError)
      this.loginError = true
    }
    return of([]);
  }
  login() {
    this.authService.login(this.loginForm.value)
      .pipe(catchError(error => {
        return this.handle(error)
      }))
      .subscribe((data: any) => {
        localStorage.setItem('token', data.token)
        this.router.navigate(['/']);
      })
  }

}
