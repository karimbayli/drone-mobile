import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(public router: Router) { }
  canActivate(): boolean {
    if (!localStorage.getItem('token')) {
      this.router.navigate(['auth/login']);
      return false
    }
    return true
  }
}
