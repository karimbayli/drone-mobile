import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CameraPhoto } from '@capacitor/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CameraServiceService {

  constructor(private http: HttpClient) { }
  private async readAsBase64(cameraPhoto: CameraPhoto) {
    // Fetch the photo, read as a blob, then convert to base64 format
    const response = await fetch(cameraPhoto.webPath!);
    const blob = await response.blob();
    return blob
  }

  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

  uploadFile(file: CameraPhoto) {
    return new Promise(async (resolve, reject) => {
      var headers = new HttpHeaders();
      new HttpHeaders({
        'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
      })
      const formData: FormData = new FormData();
      const fileName = new Date().getTime() + '.jpeg';
      const result = await this.readAsBase64(file)
      formData.append('file', result, fileName);
      console.log('upload', `${environment.host}/v1/file/upload`)
      return this.http
        .post(`${environment.host}/v1/file/upload`, formData,
          { headers: headers }
        ).subscribe(data => {
          resolve(data)
        })
    })
  }

  process(filename) {
    return this.http
      .get(`${environment.host}/v1/process/` + filename)
  }

  reportDrone(data) {
    return this.http.post(`${environment.host}/v1/drone/report`, {
      img_path: data.image,
      latitude: data.lat,
      longitude: data.lng,
      message: data.message
    })
  }

}
