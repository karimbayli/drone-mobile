import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  Plugins, CameraResultType, Capacitor, FilesystemDirectory,
  CameraPhoto, CameraSource, Geolocation
} from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { CameraServiceService } from './camera-service.service';

const { Camera, Filesystem, Storage } = Plugins;

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  public form: FormGroup;
  photo
  constructor(private cameraService: CameraServiceService,
    private alertController: AlertController,
    private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      image: [''],
      lat: [''],
      lng: [''],
      message: ['', [Validators.required]],
    })
    Geolocation.getCurrentPosition().then(position => {
      this.form.controls['lat'].setValue(position.coords.latitude)
      this.form.controls['lng'].setValue(position.coords.longitude)
    })
  }
  ngOnInit() {
  }

  public async addNewToGallery() {
    console.log('add')
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });
    this.cameraService.uploadFile(capturedPhoto).then((data: any) => {
      this.cameraService.process(data.filename).subscribe((processed: any) => {
        console.log(processed)
        this.photo = environment.host + '/v1/file/' + processed.filename
        console.log(this.form.controls)
        this.form.controls['image'].setValue(processed.filename)
      })
    })
  }

  send() {
    this.cameraService.reportDrone(this.form.value).subscribe(() => {
      this.presentAlert()
    })
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Success',
      message: 'Your report sent.',
      buttons: ['OK']
    });

    await alert.present();
  }
}
